### O (Objective): What did we learn today? What activities did you do? What scenes have impressed you?

​	今天上午先是惯例进行了Code Review。然后学习了HTTP跨域问题的原理和解决办法，然后我们学习了在前端中调用后端接口实现操作。绘制了主题为我们学到了什么前端知识的Concept Map，最后做了关于Elevator Speech&MVP的group work汇报。

### R (Reflective): Please use one word to express your feelings about today's class.

​	感动（编写后端代码）

### I (Interpretive): What do you think about this? What was the most meaningful aspect of this activity?

​	热泪盈眶，终于写到了自己更加熟悉一点的后端代码，虽然都是教过的，不算难。今天的Concept Map让我在脑海里对前端知识有了初步的模型，并且Groupwork也锻炼了我的演说能力。

### D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?

​	多写前端代码，熟悉风格。
